# Android compatible RSS 2.0 Parser 

RSS Parser that depends on a XML pull parser only, making it perfect for use in Android projects (Android [provides](https://developer.android.com/reference/org/xmlpull/v1/XmlPullParser) an implementation of a XML pull parser).

Only RSS version 2.0 is supported. See https://cyber.harvard.edu/rss/rss.html for the specification.

## Use as Java library

Make sure the [kxml2](https://mvnrepository.com/artifact/net.sf.kxml/kxml2/2.1.8) library is on the classpath.

## Use in Android project

Just drop the jar in the `libs` dir of your Android app and add the following dependency to the gradle build file:

    implementation files('libs/rssparser.jar')

## Sample code

    URLConnection urlConnection = new URL(feedUrl).openConnection();
    urlConnection.connect();
    Rss rss = new RssParser().parseDocument(urlConnection.getInputStream());
    
