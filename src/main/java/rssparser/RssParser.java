package rssparser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.xmlpull.v1.XmlPullParser.END_TAG;
import static org.xmlpull.v1.XmlPullParser.START_TAG;


public class RssParser {

    private XmlPullParser parser;
    private static boolean debug;
            
    public synchronized Rss parseDocument(InputStream inputStream) throws IOException, RssParseException {

        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);

            parser = factory.newPullParser();
            parser.setInput(inputStream, null);

            parser.nextTag();
            if (parser.getName().equalsIgnoreCase("rss"))
                return parseRss();
            else
                throw new RssParseException("no rss");
        } catch (XmlPullParserException e) {
            throw new RssParseException("rss parse exception");
        }
    }

    private Rss parseRss() throws IOException, XmlPullParserException, RssParseException {
        RssBuilder builder = new RssBuilder();

        debug("Encoding: " + parser.getInputEncoding());

        for (int i = 0; i < parser.getNamespaceCount(1); i++) {
            debug("Namespace: " + parser.getNamespacePrefix(i) + " -> " + parser.getNamespaceUri(i));
        }

        for (int i = 0; i < parser.getAttributeCount(); i++) {
            if (parser.getAttributeName(i).equalsIgnoreCase("version")) {
                builder.setVersion(parser.getAttributeValue(i));
            }
            else {
                debug("Rss attribute: " + parser.getAttributeName(i) +
                        " of type " + parser.getAttributeType(i) + " with value " + parser.getAttributeValue(i));
            }
        }

        parser.nextTag();

        RssChannel rssChannel;
        while (parser.getEventType() != END_TAG) {
            if (parser.getEventType() == START_TAG) {
                if (parser.getNamespace().equals("")) {
                    switch (parser.getName().toLowerCase()) {
                        case "channel":
                            rssChannel = parseChannel();
                            builder.add(rssChannel);
                            break;
                        default:
                            debug("invalid element in RSS");
                            throw new RssParseException("");
                    }
                }
                else {
                    debug("Rss: additional tag from " + parser.getNamespace() + " : " + parser.getName());
                    advancePastEndTag(parser.getName());
                }
            } else
                throw new RssParseException("Missing 'rss' element");

            parser.nextTag();
        }
        debug("Assert end tag == 'rss' " + (parser.getName().equalsIgnoreCase("rss")));
        return builder.build();
    }

    private RssChannel parseChannel() throws IOException, XmlPullParserException, RssParseException {
        RssChannelBuilder builder = new RssChannelBuilder();

        parser.nextTag();

        while (parser.getEventType() != END_TAG) {
            if (parser.getEventType() == START_TAG) {
                if (parser.getNamespace().equals("")) {
                    switch (parser.getName().toLowerCase()) {
                        case "title":
                            builder.setTitle(parser.nextText());
                            break;
                        case "link":
                            builder.setLink(parser.nextText());
                            break;
                        case "description":
                            builder.setDescription(parser.nextText());
                            break;
                        case "language":
                            builder.setLanguage(parser.nextText());
                            break;
                        case "copyright":
                            builder.setCopyright(parser.nextText());
                            break;
                        case "managingeditor":
                            builder.setManagingEditor(parser.nextText());
                            break;
                        case "webmaster":
                            builder.setWebMaster(parser.nextText());
                            break;
                        case "pubdate":
                            builder.setPubDate(parseDate(parser.nextText()));
                            break;
                        case "lastbuilddate":
                            builder.setLastBuildDate(parser.nextText());
                            break;
                        case "category":
                            builder.setCategory(parser.nextText());
                            break;
                        case "generator":
                            builder.setGenerator(parser.nextText());
                            break;
                        case "docs":
                            builder.setDocs(parser.nextText());
                            break;
                        case "cloud":
                            debug("Channel attribute cloud not supported");
                            break;
                        case "ttl":
                            builder.setTtl(parseNumber(parser.nextText()));
                            break;
                        case "image":
                            builder.setImage(parseImage());
                            break;
                        case "rating":
                            builder.setRating(parser.nextText());
                            break;
                        case "textInput":
                            builder.setTextInput(parser.nextText());
                            break;
                        case "skipHours":
                            builder.setSkipHours(parser.nextText());
                            break;
                        case "skipDays":
                            builder.setSkipDays(parser.nextText());
                            break;
                        case "item":
                            builder.addItem(parseItem());
                            break;
                        default:
                            debug("Unknown Channel tag " + parser.getName());
                            advancePastEndTag(parser.getName());
                    }
                }
                else {
                    debug("Channel: additional tag from " + parser.getNamespace() + " : " + parser.getName());
                    advancePastEndTag(parser.getName());
                }
                while (parser.getEventType() != END_TAG)
                    parser.next();
            }
            parser.nextTag();
        }
        debug("Assert end tag == 'channel' " + (parser.getName().equalsIgnoreCase("channel")));
        return builder.build();
    }

    private RssImage parseImage() throws IOException, XmlPullParserException {
        RssImageBuilder builder = new RssImageBuilder();

        parser.nextTag();
        while (parser.getEventType() != END_TAG) {
            if (parser.getEventType() == START_TAG) {
                if (parser.getNamespace().equals("")) {
                    switch (parser.getName().toLowerCase()) {
                        case "url":
                            builder.setUrl(parser.nextText());
                            break;
                        case "title":
                            builder.setTitle(parser.nextText());
                            break;
                        case "link":
                            builder.setLink(parser.nextText());
                            break;
                        case "width":
                            builder.setWidth(parseNumber(parser.nextText()));
                            break;
                        case "height":
                            builder.setHeight(parseNumber(parser.nextText()));
                            break;
                        case "description":
                            builder.setDescription(parser.nextText());
                            break;
                        default:
                            debug("Image tag " + parser.getName());
                            advancePastEndTag(parser.getName());
                    }
                }
                else {
                    debug("Image: additional tag from " + parser.getNamespace() + " : " + parser.getName());
                    advancePastEndTag(parser.getName());
                }
                while (parser.getEventType() != END_TAG)
                    parser.next();
            }

            parser.nextTag();
        }
        debug("Assert end tag == 'image' " + (parser.getName().equalsIgnoreCase("image")));
        return builder.build();
    }

    private RssItem parseItem() throws IOException, XmlPullParserException, RssParseException {
        RssItemBuilder builder = new RssItemBuilder();

        parser.nextTag();
        while (parser.getEventType() != END_TAG) {
            if (parser.getEventType() == START_TAG) {
                if (parser.getNamespace().equals("")) {
                    switch (parser.getName().toLowerCase()) {
                        case "title":
                            builder.setTitle(parser.nextText());
                            break;
                        case "link":
                            builder.setLink(parser.nextText());
                            break;
                        case "description":
                            builder.setDescription(parser.nextText());
                            break;
                        case "author":
                            builder.setAuthor(parser.nextText());
                            break;
                        case "category":
                            builder.addCategory(parser.nextText());
                            break;
                        case "comments":
                            builder.setComments(parser.nextText());
                            break;
                        case "enclosure":
                            builder.setEnclosure(parseEnclosure());
                            break;
                        case "guid":
                            builder.setGuid(parser.nextText());
                            break;
                        case "pubdate":
                            builder.setPubDate(parseDate(parser.nextText()));
                            break;
                        case "source":
                            builder.setSource(parser.nextText());
                            break;
                        default:
                            debug("Item tag " + parser.getName());
                            advancePastEndTag(parser.getName());
                    }
                }
                else {
                    debug("Item: additional tag from " + parser.getNamespace() + " : " + parser.getName());
                    advancePastEndTag(parser.getName());
                }

                while (parser.getEventType() != END_TAG)
                    parser.next();
            }

            parser.nextTag();
        }
        debug("Assert end tag == 'item' " + (parser.getName().equalsIgnoreCase("item")));
        return builder.build();
    }

    private Enclosure parseEnclosure() throws IOException, XmlPullParserException {
        EnclosureBuilder builder = new EnclosureBuilder();

        for (int i = 0; i < parser.getAttributeCount(); i++) {
            switch (parser.getAttributeName(i)) {
                case "url":
                    builder.setUrl(parser.getAttributeValue(i));
                    break;
                case "length":
                    builder.setLength(parseNumber(parser.getAttributeValue(i)));
                    break;
                case "type":
                    builder.setType(parser.getAttributeValue(i));
                    break;
            }
        }

        while (parser.getEventType() != END_TAG)
            parser.next();

        return builder.build();
    }

    private void advancePastEndTag(String tag) throws IOException, XmlPullParserException {
        int level = 1;
        do {
            parser.next();
            if (parser.getEventType() == START_TAG)
                level++;
            else if (parser.getEventType() == END_TAG)
                level--;
        }
        while (level > 0 || (parser.getEventType() != END_TAG || (parser.getEventType() == END_TAG && !parser.getName().equals(tag))));
    }

    private Date parseDate(String text) throws RssParseException {
        try {
            return new SimpleDateFormat("E, d MMM yyyy HH:mm:ss Z", Locale.US).parse(text);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new RssParseException("Invalid date format");
        }
    }

    private Number parseNumber(String text) {
        return Integer.parseInt(text);
    }

    private static void debug(String msg) {
        if (debug)
            System.out.println(msg);
    }
}
