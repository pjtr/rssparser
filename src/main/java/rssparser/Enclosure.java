package rssparser;

public class Enclosure {

    private final String url;
    private final Number length;
    private final String type;

    public Enclosure(String url, Number length, String type) {
        this.url = url;
        this.length = length;
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public Number getLength() {
        return length;
    }

    public String getType() {
        return type;
    }
}
