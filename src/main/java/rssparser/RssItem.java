package rssparser;

import java.util.Date;
import java.util.List;

public class RssItem {

    private final String title;
    private final String link;
    private final String description;
    private final String author;
    private final List<String> categories;
    private final String comments;
    private final Enclosure enclosure;
    private final String guid;
    private final Date pubDate;
    private final String source;

    public RssItem(String title, String link, String description, String author, List<String> categories,
                   String comments, Enclosure enclosure, String guid, Date pubDate, String source) {

        this.title = title;
        this.link = link;
        this.description = description;
        this.author = author;
        this.categories = categories;
        this.comments = comments;
        this.enclosure = enclosure;
        this.guid = guid;
        this.pubDate = pubDate;
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

    public String getAuthor() {
        return author;
    }

    public List<String> getCategories() {
        return categories;
    }

    public String getComments() {
        return comments;
    }

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public String getGuid() {
        return guid;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public String getSource() {
        return source;
    }
}
