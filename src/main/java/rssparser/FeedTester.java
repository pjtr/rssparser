package rssparser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class FeedTester {

    public static void main(String[] args) throws RssParseException {
        try {

            String[] feedUrls = new String[] {
                    "http://feeds.gimletmedia.com/hearreplyall",
                    "https://techsnacks.podbean.com/feed.xml",
                    "http://feeds2.feedburner.com/frankwatching/",
                    "http://hetiskoers.nl/category/podcast/feed/",
                    "http://feeds.nos.nl/nosop3-tech-podcast",
                    "https://rss.art19.com/de-rode-lantaarn",
                    "https://podcast.npo.nl/feed/de-kopgroep.xml",
                    "http://rss.art19.com/de-week-van-nutech",
                    "https://anchor.fm/s/5828b9c/podcast/rss",
                    "https://rss.art19.com/haagse-zaken",
                    "https://rss.art19.com/de-week-van-nutech",
                    "https://www.bnr.nl/programmas/wetenschap-vandaag?widget=rssfeed&view=podcastFeed&contentId=502",
                    "https://podcast.npo.nl/feed/de-dag.xml",
                    "https://podcast.npo.nl/feed/destemmingvanvullingsenvanweezel.xml",
                    "https://rss.art19.com/eeuw-van-de-amateur",
                    "https://rss.art19.com/een-podcast-over-media",
                    "https://rss.art19.com/tweakers-podcast",
                    "http://feeds.feedburner.com/BNRDeTechnoloog",
                    "https://anchor.fm/s/5828b9c/podcast/rss",
                    "http://www.tech45.eu/wordpress/feeds/tech45_podcast.xml",
                    "https://www.wired.co.uk/rss/podcast",
                    "http://feeds.gimletmedia.com/hearreplyall",
                    "https://www.bnr.nl/podcast/digitaal?widget=rssfeed&view=podcastFeed",
                    "https://www.bnr.nl/podcast/wetenschap-vandaag?widget=rssfeed",
                    "https://rss.art19.com/onbehaarde-apen",
                    "https://podcast.npo.nl/feed/dekennisvannu.xml",
                    "http://universityofgroningen.libsyn.com/rss",
                    "https://cassettepodcast.podbean.com/feed.xml",
                    "https://rss.art19.com/onbehaarde-apen",
                    "https://techsnacks.nl/feed/",
            };

            //parseFeed(feedUrls[4]);

            for (String feedUrl: feedUrls) {
                parseFeed(feedUrl);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void parseFeed(String feedUrl) throws IOException, RssParseException {
        URLConnection urlConnection = new URL(feedUrl).openConnection();
        urlConnection.connect();

        System.out.println("URL: " + feedUrl);
        Rss rss = new RssParser().parseDocument(urlConnection.getInputStream());
        if (!rss.getVersion().equals("2.0")) {
            System.out.println("NOT RSS 2.0!   -> " + feedUrl);
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }
}
