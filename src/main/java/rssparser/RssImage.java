package rssparser;

public class RssImage {

    private final String url;
    private final String title;
    private final String link;
    private final Number width;
    private final Number height;
    private final String description;

    public RssImage(String url, String title, String link, Number width, Number height, String description) {
        this.url = url;
        this.title = title;
        this.link = link;
        this.width = width;
        this.height = height;
        this.description = description;


        System.out.println("RSS Image URL = " + url);
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public Number getWidth() {
        return width;
    }

    public Number getHeight() {
        return height;
    }

    public String getDescription() {
        return description;
    }
}
