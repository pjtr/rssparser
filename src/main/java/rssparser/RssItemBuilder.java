package rssparser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RssItemBuilder {

    private String title;
    private String link;
    private String description;
    private String author;
    private List<String> categories = new ArrayList<>();
    private String comments;
    private Enclosure enclosure;
    private String guid;
    private Date pubDate;
    private String source;

    public RssItem build() {
        return new RssItem(title, link, description, author, categories, comments, enclosure, guid, pubDate, source);
    }

    public RssItemBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public RssItemBuilder setLink(String link) {
        this.link = link;
        return this;
    }

    public RssItemBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public RssItemBuilder setAuthor(String author) {
        this.author = author;
        return this;
    }

    public RssItemBuilder addCategory(String category) {
        this.categories.add(category);
        return this;
    }

    public RssItemBuilder setComments(String comments) {
        this.comments = comments;
        return this;
    }

    public RssItemBuilder setEnclosure(Enclosure enclosure) {
        this.enclosure = enclosure;
        return this;
    }

    public RssItemBuilder setGuid(String guid) {
        this.guid = guid;
        return this;
    }

    public RssItemBuilder setPubDate(Date pubDate) {
        this.pubDate = pubDate;
        return this;
    }

    public RssItemBuilder setSource(String source) {
        this.source = source;
        return this;
    }
}
