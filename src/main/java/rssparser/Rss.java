package rssparser;

public class Rss {

    private final String version;
    private final RssChannel rssChannel;

    public Rss(String version, RssChannel rssChannel) {

        this.version = version;
        this.rssChannel = rssChannel;
    }

    public String getVersion() {
        return version;
    }

    public RssChannel getRssChannel() {
        return rssChannel;
    }
}
