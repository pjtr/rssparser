package rssparser;

public class RssParseException extends Exception {

    public RssParseException(String message) {
        super(message);
    }
}
