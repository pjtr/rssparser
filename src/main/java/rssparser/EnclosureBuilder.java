package rssparser;

public class EnclosureBuilder {

    private String url;
    private Number length;
    private String type;

    public Enclosure build() {
        return new Enclosure(url, length, type);
    }

    public EnclosureBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public EnclosureBuilder setLength(Number length) {
        this.length = length;
        return this;
    }

    public EnclosureBuilder setType(String type) {
        this.type = type;
        return this;
    }
}
