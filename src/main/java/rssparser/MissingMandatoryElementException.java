package rssparser;

public class MissingMandatoryElementException extends RssParseException {

    public MissingMandatoryElementException(String element, String child) {
        super("Missing mandatory element '" + child + "' in " + element);
    }
}
