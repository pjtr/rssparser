package rssparser;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class RssChannel {

    private final String title;
    private final String link;
    private final String description;
    private final List items;
    private final String language;
    private final String copyright;
    private final String webMaster;
    private final Date pubDate;
    private final String buildDate;
    private final String category;
    private final String generator;
    private final String docs;
    private final Number ttl;
    private final RssImage rssImage;
    private final String rating;
    private final String textInput;
    private final String skipHours;
    private final String skipDays;
    private final String managingEditor;


    RssChannel(String title, String link, String description, List items, String language, String copyright,
                      String webMaster, Date pubDate, String buildDate, String category, String generator, String docs,
                      Number ttl, RssImage rssImage, String rating, String textInput, String skipHours, String skipDays,
                      String managingEditor) throws RssParseException {

        if (title == null || title.trim().equals(""))
            throw new MissingMandatoryElementException("Channel", "title");
        if (link == null || link.trim().equals(""))
            throw new MissingMandatoryElementException("Channel", "link");
        if (description == null || description.trim().equals(""))
            throw new MissingMandatoryElementException("Channel", "description");

        this.title = title;
        this.link = link;
        this.description = description;
        this.items = Collections.unmodifiableList(items);

        this.language = language;
        this.copyright = copyright;
        this.webMaster = webMaster;
        this.pubDate = pubDate;
        this.buildDate = buildDate;
        this.category = category;
        this.generator = generator;
        this.docs = docs;
        this.ttl = ttl;
        this.rssImage = rssImage;
        this.rating = rating;
        this.textInput = textInput;
        this.skipHours = skipHours;
        this.skipDays = skipDays;
        this.managingEditor = managingEditor;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

    public List<RssItem> getItems() {
        return items;
    }

    public String getLanguage() {
        return language;
    }

    public String getCopyright() {
        return copyright;
    }

    public String getWebMaster() {
        return webMaster;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public String getBuildDate() {
        return buildDate;
    }

    public String getCategory() {
        return category;
    }

    public String getGenerator() {
        return generator;
    }

    public String getDocs() {
        return docs;
    }

    public Number getTtl() {
        return ttl;
    }

    public RssImage getRssImage() {
        return rssImage;
    }

    public String getRating() {
        return rating;
    }

    public String getTextInput() {
        return textInput;
    }

    public String getSkipHours() {
        return skipHours;
    }

    public String getSkipDays() {
        return skipDays;
    }

    public String getManagingEditor() {
        return managingEditor;
    }
}
