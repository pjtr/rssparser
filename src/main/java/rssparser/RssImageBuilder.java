package rssparser;

public class RssImageBuilder {

    private String url;
    private String title;
    private String link;
    private Number width;
    private Number height;
    private String description;

    public RssImage build() {
        return new RssImage(url, title, link, width, height, description);
    }

    public RssImageBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public RssImageBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public RssImageBuilder setLink(String link) {
        this.link = link;
        return this;
    }

    public RssImageBuilder setWidth(Number width) {
        this.width = width;
        return this;
    }

    public RssImageBuilder setHeight(Number height) {
        this.height = height;
        return this;
    }

    public RssImageBuilder setDescription(String description) {
        this.description = description;
        return this;
    }
}
