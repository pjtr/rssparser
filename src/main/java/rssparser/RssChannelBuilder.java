package rssparser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RssChannelBuilder {

    private List items = new ArrayList();
    private String title;
    private String link;
    private String description;
    private String language;
    private String copyright;
    private String webMaster;
    private Date pubDate;
    private String buildDate;
    private String category;
    private String generator;
    private String docs;
    private Number ttl;
    private RssImage rssImage;
    private String rating;
    private String textInput;
    private String skipHours;
    private String skipDays;
    private String managingEditor;

    public RssChannel build() throws RssParseException {
        return new RssChannel(title, link, description, items,
                language, copyright, webMaster, pubDate, buildDate, category, generator, docs, ttl,
                rssImage, rating, textInput, skipHours, skipDays, managingEditor);
    }

    public RssChannelBuilder addItem(RssItem rssItem) {
        items.add(rssItem);
        return this;
    }

    public RssChannelBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public RssChannelBuilder setLink(String link) {
        this.link = link;
        return this;
    }

    public RssChannelBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public RssChannelBuilder setLanguage(String language) {
        this.language = language;
        return this;
    }

    public RssChannelBuilder setCopyright(String copyright) {
        this.copyright = copyright;
        return this;
    }

    public RssChannelBuilder setManagingEditor(String managingEditor) {
        this.managingEditor = managingEditor;
        return this;
    }

    public RssChannelBuilder setWebMaster(String webMaster) {
        this.webMaster = webMaster;
        return this;
    }

    public RssChannelBuilder setPubDate(Date pubDate) {
        this.pubDate = pubDate;
        return this;
    }

    public RssChannelBuilder setLastBuildDate(String buildDate) {
        this.buildDate = buildDate;
        return this;
    }

    public RssChannelBuilder setCategory(String category) {
        this.category = category;
        return this;
    }

    public RssChannelBuilder setGenerator(String generator) {
        this.generator = generator;
        return this;
    }

    public RssChannelBuilder setDocs(String docs) {
        this.docs = docs;
        return this;
    }

    public RssChannelBuilder setTtl(Number ttl) {
        this.ttl = ttl;
        return this;
    }

    public RssChannelBuilder setImage(RssImage rssImage) {
        this.rssImage = rssImage;
        return this;
    }

    public RssChannelBuilder setRating(String rating) {
        this.rating = rating;
        return this;
    }

    public RssChannelBuilder setTextInput(String textInput) {
        this.textInput = textInput;
        return this;
    }

    public RssChannelBuilder setSkipHours(String skipHours) {
        this.skipHours = skipHours;
        return this;
    }

    public RssChannelBuilder setSkipDays(String skipDays) {
        this.skipDays = skipDays;
        return this;
    }
}
