package rssparser;

public class RssBuilder {

    private String version;
    private RssChannel rssChannel;

    public RssBuilder add(RssChannel rssChannel) {
        this.rssChannel = rssChannel;
        return this;
    }

    public Rss build() {
        return new Rss(version, rssChannel);
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
